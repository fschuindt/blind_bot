module FoodBot
  private
    def food_bot
      loop do
        @action_queue << lambda {
          food_formula = @formula[:food]
          6.times do
            @interface.right_click(food_formula[:pos])
          end
        }
        sleep(150)
      end
    end
end
