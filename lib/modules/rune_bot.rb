module RuneBot
  private
    def rune_bot
      @rune_formula = @formula[:rune]
      mana_time = calculate_mana_time
      @rune_formula[:bps_to_make].to_i.times do |bp_i|
        20.times do |r_i|
          @action_queue << lambda {
            @interface.drag(@rune_formula[:blanks][bp_i], @rune_formula[:hand])
            @interface.type(@rune_formula[:spell_word])
            @interface.drag(@rune_formula[:hand], @rune_formula[:stores][bp_i])
          }
          sleep(mana_time)
        end

        log "A backpack was completed, #{bp_i+1} in total."
      end

      log "Rune Bot: Process finished."
      send_exit_signal if @rune_formula[:logout]
    end

    def calculate_mana_time
      mana = @spells_mana[@rune_formula[:spell_word]]
      if @rune_formula[:promoted]
        if @rune_formula[:vocation] == 'm'
          (4 * mana) + 10
        else
          (6 * mana) + 10
        end
      else
        if @rune_formula[:vocation] == 'm'
          (6 * mana) + 10
        else
          (8 * mana) + 10
        end
      end
    end
end
