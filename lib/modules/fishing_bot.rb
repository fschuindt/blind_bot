module FishingBot
  private
    def fishing_bot
      loop do
        @action_queue << lambda {
          fishing_formula = @formula[:fishing]
          fishing_formula[:loops].times do
            fishing_formula[:sqms].count.times do |i|
              @interface.right_click(fishing_formula[:rod], 5)
              @interface.click(fishing_formula[:sqms][i], 6)
            end
          end
        }
        sleep(200)
      end
    end
end
