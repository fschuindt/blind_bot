require 'fileutils'

module PresenceDetector
  private
    MASTER_PATH = 'tmp/images/master.png'
    SLAVE_PATH = 'tmp/images/slave.png'
    def presence_detector
      print_battle_master
      loop do
        print_battle_slave
        fire_alarm if slave_changed?
        sleep 1
      end
    end

    def print_battle_master
      File.delete(MASTER_PATH) if File.exist?(MASTER_PATH)
      @interface.capture(@formula[:battle_window][:area], MASTER_PATH)
      @master = File.binread(MASTER_PATH)
    end

    def print_battle_slave
      File.delete(SLAVE_PATH) if File.exist?(SLAVE_PATH)
      @interface.capture(@formula[:battle_window][:area], SLAVE_PATH)
    end

    def slave_changed?
      @slave = File.binread(SLAVE_PATH)
      @master != @slave
    end

    def fire_alarm
      log "Presence Detector: Alarm fired."
      @last_battle_window = "logs/#{time_stamp(:file_tag)}.png"
      FileUtils.cp SLAVE_PATH, @last_battle_window
      @interface.mass_logout
      send_exit_signal
    end
end
