require 'json'
require 'hashie'

module Formula
  private
    def load_formula
      print "Formula name: "
      file_name = gets.chomp
      @formula = JSON.parse(File.read("formulas/#{file_name}"))
      @spells_mana = JSON.parse(File.read('lib/spells_mana.json'))
      Hashie.symbolize_keys! @formula
      puts "#{file_name} loaded with success."
      print "\n"
    end

    def create_formula
      @building_formula = {}

      print "\n"
      puts "Modules: [1] Fishing, [2] Rune Making, [3] Food Eating, [4] Anti Logout, [5] Presence Detector."
      print "> Type a list of Modules numbers separated by commas (Eg.: 2, 3): "
      @building_formula[:modules] = []
      modules_list = gets.chomp
      moludes_list = modules_list.split(',')
      moludes_list.each do |module_id|
        @building_formula[:modules] << module_id.to_i
      end

      @building_formula[:modules].each do |module_id|
        case module_id
        when 1
          create_fishing_formula
        when 2
          create_rune_formula
        when 3
          create_food_formula
        when 5
          select_battle_window
        end
      end

      print "Choose a file name: "
      file_name = gets.chomp
      print "\n"

      File.open("formulas/#{file_name}", "w") do |f|
        f.write(@building_formula.to_json)
      end

      puts "Formula creation complete."
      print "\n"
    end

    def create_fishing_formula
      print "\n"
      puts "Starting fishing formula."
      @building_formula[:fishing] = {}
      print "\n"

      print "> How many fishing SQMs?: "
      fishing_sqms = gets.chomp.to_i
      print "\n"

      print "> Fishing loops per cycle: "
      @building_formula[:fishing][:loops] = gets.chomp.to_i
      print "\n"

      if fishing_sqms > 0
        puts "- Map your Fishing Rod now..."
        @building_formula[:fishing][:rod] = map_pos
        puts "- Map #{fishing_sqms} fishing SQMs. Starts in 4 seconds...\n\n"
        sleep(4)
        puts "Start!\n\n"
        @building_formula[:fishing][:sqms] = []
        fishing_sqms.times do |count|
          @building_formula[:fishing][:sqms] << map_fishing_pos(count+1)
        end
      end

      print "\n"
      puts "Fishing formula is done."
    end

    def create_rune_formula
      print "\n"
      puts "Starting rune making formula."
      @building_formula[:rune] = {}
      print "\n"

      print "> Mage or Paladin? (m/p): "
      @building_formula[:rune][:vocation] = gets.chomp

      print "\n"
      print "> Promoted? (y/n): "
      if gets.chomp == 'y'
        @building_formula[:rune][:promoted] = true
      else
        @building_formula[:rune][:promoted] = false
      end

      print "\n"
      puts "Eg.: adura vita"
      print "> Spell Word: "
      @building_formula[:rune][:spell_word] = gets.chomp

      print "\n"
      print "> How many BPs to make?: "
      @building_formula[:rune][:bps_to_make] = gets.chomp.to_i

      print "\n"
      puts "Ok, now we're going to map the positions, get ready...\n\n"
      puts "- Free Hand Slot..."
      @building_formula[:rune][:hand] = map_pos

      @building_formula[:rune][:blanks] = []
      @building_formula[:rune][:stores] = []
      @building_formula[:rune][:bps_to_make].times do |i|
        puts "- Blank Rune Slot BP-#{i+1}..."
        @building_formula[:rune][:blanks] << map_pos

        puts "- Rune Store Slot BP-#{i+1}..."
        @building_formula[:rune][:stores] << map_pos
      end

      print "\n"
      print "> Logout after finished? (y/n): "
      if gets.chomp == 'y'
        @building_formula[:rune][:logout] = true
      else
        @building_formula[:rune][:logout] = false
      end

      print "\n"
      puts "Rune making formula is done."
      print "\n"
    end

    def create_food_formula
      print "\n"
      puts "Starting food eating formula."
      @building_formula[:food] = {}
      print "\n"

      puts "- Map your food position..."
      @building_formula[:food][:pos] = map_pos

      puts "Food eating formula is done."
      print "\n"
    end

    def select_battle_window
      print "\n"
      puts "Select the 'Battle' window area."
      area = @interface.getarea
      @building_formula[:battle_window] = {}
      @building_formula[:battle_window][:area] = area

      puts ":: Selected '#{area}'."
      print "\n"

      puts "'Battle' window area is saved."
      print "\n"
    end

    def map_fishing_pos(count)
      pos = @interface.getpos
      puts ":: #{count} - Mapped #{pos[0]}, #{pos[1]}.\n"
      sleep(2)
      {:x => pos[0], :y => pos[1]}
    end

    def map_pos
      sleep(4)
      pos = @interface.getpos
      puts ":: Mapped #{pos[0]}, #{pos[1]}.\n\n"
      {:x => pos[0], :y => pos[1]}
    end
end
