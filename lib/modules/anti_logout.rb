module AntiLogout
  private
    def anti_logout
      sleep 10
      loop do
        @action_queue << lambda {
          @interface.type("a")
          sleep 1
          @interface.send_input("{UP}")
          sleep 2
          @interface.send_input("{DOWN}")
        }
        sleep 700
      end
    end
end
