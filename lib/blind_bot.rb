require 'dotenv'
require 'thread'
require 'mail'

Dotenv.load

require_relative 'interfaces/linux_interface'
require_relative 'interfaces/windows_interface'
require_relative 'modules/formula'
require_relative 'modules/rune_bot'
require_relative 'modules/fishing_bot'
require_relative 'modules/food_bot'
require_relative 'modules/anti_logout'
require_relative 'modules/presence_detector'

class BlindBot

  include Formula
  include RuneBot
  include FishingBot
  include FoodBot
  include AntiLogout
  include PresenceDetector

  def initialize
    @interface = interface
    @exit = false
  end

  def run
    loop do
      command = gets.chomp.strip
      case command
      when 'create formula'
        create_formula
      when 'start bot'
        load_formula
        start_modules
        start_bot
      else
        puts "Command not found."
        print "\n"
      end
    end
  end

  private
    def start_bot
      @main_thread = Thread.current.object_id
      setup_mailer
      sleep(5)

      loop do
        action = @action_queue.shift
        action.call
      end
    end

    def start_modules
      initlog
      log "Started."

      @action_queue = Queue.new
      Thread.new { exit_on_signal }

      @formula[:modules].each do |m|
        case m
        when 1
          Thread.new { fishing_bot }
        when 2
          Thread.new { rune_bot }
        when 3
          Thread.new { food_bot }
        when 4
          Thread.new { anti_logout }
        when 5
          Thread.new { presence_detector }
        else
          puts "Module '#{m}' not found."
        end
      end
    end

    def interface
      @is_windows = Gem.win_platform?
      unless @is_windows
        LinuxInterface.new
      else
        WindowsInterface.new
      end
    end

    def exit_on_signal
      loop do
        sleep 1
        if @exit
          stop_modules
          @interface.mass_logout
          log "Exited."
          log_file = File.read(@log_file)
          send_email("BlindBot Exited", log_file, @last_battle_window)
          exit true
        end
      end
    end

    def send_exit_signal
      @exit = true
    end

    def initlog
      @log_file = "logs/#{time_stamp(:file_tag)}.log"
    end

    def log(message)
      stamp = time_stamp(:normal)
      log_message = stamp + " | " + message + "\n"
      File.open(@log_file, 'a') { |f| f.write(log_message) }
    end

    def time_stamp(mode=:normal)
      stamp = Time.now.strftime("%m-%d-%Y %I:%M:%S%P")
      if mode == :normal
        stamp
      elsif mode == :file_tag
        stamp.gsub(' ', '_').gsub(':', '-')
      end
    end

    def setup_mailer
      Mail.defaults do
        delivery_method :smtp, {
          :address => ENV['SMTP_ADDRESS'],
          :port => ENV['SMTP_PORT'],
          :domain => ENV['SMTP_DOMAIN'],
          :user_name => ENV['SMTP_USER_NAME'],
          :password => ENV['SMTP_PASSWORD'],
          :authentication => 'login',
          :enable_starttls_auto => true
        }
      end
    end

    def send_email(subj, msg, att=nil)
      Mail.deliver do
        to ENV['SMTP_EMAIL_TO']
        from "Notification <#{ENV['SMTP_EMAIL_FROM']}>"
        subject subj
        add_file att
        text_part do
          body msg
        end
      end
    end

    def stop_modules
      Thread.list.each do |thread|
        if thread != Thread.current and thread.object_id != @main_thread
          thread.exit
        end
      end
    end
end

blind_bot = BlindBot.new
blind_bot.run
