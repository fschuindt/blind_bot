class LinuxInterface
  def initialize
    require "xdo/mouse"
    require "xdo/keyboard"
    require "xdo/xwindow"
  end
  def getarea
    `lib/slop.sh`.gsub("\n",'')
  end
  def capture(area, name='')
    area = area.split(' / ')
    g = area[0]
    id = area[1]
    `maim -g "#{g}" -i "#{id}" #{name}`
  end
  def getpos
    XDo::Mouse.position
  end
  def click(pos, speed=2)
    sleep(1)
    XDo::Mouse.click(pos[:x], pos[:y], :left, speed)
  end
  def right_click(pos, speed=2)
    sleep(1)
    XDo::Mouse.click(pos[:x], pos[:y], :right, speed)
  end
  def drag(pos_from, pos_to)
    sleep(1)
    XDo::Mouse.drag(pos_from[:x], pos_from[:y], pos_to[:x], pos_to[:y])
  end
  def type(string)
    sleep(1)
    XDo::Keyboard.simulate("#{string}{RETURN}")
  end
  def send_input(string)
    sleep(1)
    XDo::Keyboard.simulate(string)
  end
  def logout
    XDo::Keyboard.simulate("{CTRL+L}")
    sleep(1)
  end
  def mass_logout
    4.times { logout }
  end
end
