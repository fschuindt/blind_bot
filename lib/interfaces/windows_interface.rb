class WindowsInterface
  def initialize
    require "au3"
  end
  def getarea
    bc = `lib/boxcutter.exe`
    bc = bc.split("\n")
    area = bc[0].gsub('screenshot coords: ', '').
      gsub('(', '').gsub(')', '').gsub('-', ',')
  end
  def capture(area, name='')
    bc = `lib/boxcutter.exe -c #{area} #{name}`
  end
  def getpos
    AutoItX3.cursor_pos
  end
  def click(pos, speed=10)
    sleep(1)
    AutoItX3.mouse_click(pos[:x], pos[:y], "Primary", 1, speed)
  end
  def right_click(pos, speed=10)
    sleep(1)
    AutoItX3.mouse_click(pos[:x], pos[:y], "Secondary", 1, speed)
  end
  def drag(pos_from, pos_to)
    sleep(1)
    AutoItX3.drag_mouse(pos_from[:x], pos_from[:y], pos_to[:x], pos_to[:y], "Primary", 10)
  end
  def type(string)
    sleep(1)
    AutoItX3.send_keys("#{string}{ENTER}")
  end
  def send_input(string)
    sleep(1)
    AutoItX3.send_keys(string)
  end
  def logout
    5.times { AutoItX3.send_keys("^l") }
    sleep(1)
  end
  def mass_logout
    4.times { logout }
  end
end
